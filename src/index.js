import React from 'react';
import ReactDOM from 'react-dom';
import './App.css';
import App from './App';
import Chatbot from './Components/ChatBot';

ReactDOM.render(

  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
