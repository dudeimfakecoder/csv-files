import React, { useRef } from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import NavBar from './NavBar';
import Home from './Components/Home';
import About from './Components/About';
import Projects from './Components/Projects';
import Certificates from './Components/Certificates';

const theme = createTheme({
  palette: {
    background: {
      default: '#000000', // Set to black (#000000)
    },
  },
});

function App() {
  const aboutRef = useRef(null);
  const projectsRef = useRef(null);
  const certificatesRef = useRef(null);

  const scrollToSection = (ref) => {
    ref.current.scrollIntoView({ behavior: 'smooth' });
  };

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Router>
        <NavBar
          onAboutClick={() => scrollToSection(aboutRef)}
          onProjectsClick={() => scrollToSection(projectsRef)}
          onCertificatesClick={() => scrollToSection(certificatesRef)}
        />
        <Routes>
          <Route
            path="/"
            element={<Home />}
          />
          <Route
            path="/about"
            element={<About />}
          />
          <Route
            path="/projects"
            element={<Projects />}
          />
          <Route
            path="/certificates"
            element={<Certificates />}
          />
        </Routes>
      </Router>
    </ThemeProvider>
  );
}

export default App;
