import React from 'react';

const Projects = () => {
  // Create an array of project objects with title, description, and links
  const projects = [
    {
      title: 'Project 1',
      description: 'Description of your first project.',
      link: 'Link to project 1',
    },
    {
      title: 'Project 2',
      description: 'Description of your second project.',
      link: 'Link to project 2',
    },
    // Add more projects as needed
  ];

  return (
    <div className="projects">
      <h2>Projects</h2>
      {projects.map((project, index) => (
        <div key={index} className="project">
          <h3>{project.title}</h3>
          <p>{project.description}</p>
          <a href={project.link} target="_blank" rel="noopener noreferrer">
            View Project
          </a>
        </div>
      ))}
    </div>
  );
}

export default Projects;
