import React from 'react';
import { Button, Container, Paper, Typography } from '@mui/material';
import { Widget, addResponseMessage } from 'react-chat-widget';
import { useEffect } from 'react';
import 'react-chat-widget/lib/styles.css';
//import logo from './logo.svg';
import image from '../../src/images/Arun_image.jpeg';
import { motion } from "framer-motion";

const Home = () => {

    useEffect(() => {
        addResponseMessage('Welcome to this **awesome** chat!');
    }, []);

    const handleNewUserMessage = (newMessage) => {
        console.log(`New message incoming! ${newMessage}`);
        // Now send the message throught the backend API
    };

    const [move, setMove] = React.useState(false);


    return (
        <React.Fragment>
            <div className="column" style={{ display: 'grid', placeItems: 'center', position: 'relative', top: '56px' }}>
                <h1 className='titleName title titlesize'>Designer &amp; Developer</h1>
                <h2 class="subtitle is-size-4-tablet titleName title">I design and code beautifully simple things, and I love what I do.</h2>

                <div className="avatar">
                    <img
                        src="https://mattfarley.ca/img/mf-avatar.svg"
                        alt="Avatar"
                        style={{ width: '100px', height: '100px' }}
                        onClick={() => {
                            setMove(!move);
                        }}
                    />
                </div>
            </div>
            <div class="">
                <div class="home-hero__social">
                    <a href="https://www.linkedin.com/in/arun-%C2%A0m-9163181a0/" class="home-hero__social-icon-link" rel="noreferrer" target="_blank">
                        <img src="https://d33wubrfki0l68.cloudfront.net/d8e6e1e636531e28274a1b8b6d947b817f6145bd/d42d3/assets/svg/linkedin-dark.svg" alt="Arun Linkedin Profile" class="home-hero__social-icon" />
                    </a>
                </div>
                <div class="home-hero__social">
                    <a href="https://twitter.com/rammcodes" class="home-hero__social-icon-link" rel="noreferrer" target="_blank">
                        <img src="https://d33wubrfki0l68.cloudfront.net/ba3ef9fd9d500c55ba265adae6087ba71e05f5d9/d0067/assets/svg/twitter-dark.svg" alt="RammCodes Twitter Profile" class="home-hero__social-icon" />
                    </a>
                </div>
                <div class="home-hero__social">
                    <a href="https://www.youtube.com/channel/UCIb8_J9lqtiWGMIOdQ5DVsg" class="home-hero__social-icon-link home-hero__social-icon-link--bd-none" rel="noreferrer" target="_blank">
                        <img src="https://d33wubrfki0l68.cloudfront.net/7c95be3350552a5e9f2fc47f1bdbac118ea4dcee/f7a5e/assets/svg/yt-dark.svg" alt="RammCodes Youtube Channel" class="home-hero__social-icon" />
                    </a>
                </div>
                <div class="home-hero__social">
                    <a href="https://github.com/rammcodes" class="home-hero__social-icon-link" rel="noreferrer" target="_blank">
                        <img src="https://d33wubrfki0l68.cloudfront.net/38469cf88d038b6ba3322c9fcb93a8f7167df4b9/cb0b9/assets/svg/github-dark.svg" alt="RammCodes Github Profile" class="home-hero__social-icon" />
                    </a>
                </div>
                <Widget
                    handleNewUserMessage={handleNewUserMessage}
                    title="My new awesome title"
                    profileAvatar={image}
                    subtitle="And my cool subtitle"
                />
            </div>
        </React.Fragment >

    );
};

export default Home;
