// Section.js
import React from 'react';

const Section = ({ id }) => {
  return (
    <div id={id} style={{ height: '500px', border: '1px solid black' }}>
      <h2>{id}</h2>
      {/* Add content for each section */}
    </div>
  );
};

export default Section;
