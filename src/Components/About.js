import { Paper, Typography, Box, Button, Grid } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { motion, useAnimation } from 'framer-motion';

const About = () => {
    const backgroundControls = useAnimation();
    const controls = useAnimation();
    const skillsControls = useAnimation();
    const [isHovered, setIsHovered] = useState(false);


    // Animation configuration
    const animationConfig = {
        rotate: [0, 360], // Rotate from 0 to 360 degrees
        transition: { duration: 2, repeat: Infinity }, // Repeat the animation continuously
    };

    // Animation configuration for background
    const backgroundAnimationConfig = {
        x: ['-100%', '0%'], // Move from -100% to 0% on the x-axis
        transition: { duration: 5, repeat: Infinity, repeatType: 'reverse' }, // Repeat the animation continuously
    };

    // Animation configuration for skills
    const skillsAnimationConfig = {
        rotate: [0, 360], // Rotate from 0 to 360 degrees
        transition: { duration: 2, repeat: Infinity }, // Repeat the animation continuously
    };

    // Start the background animation when the component mounts
    useEffect(() => {
        backgroundControls.start(backgroundAnimationConfig);
    }, [backgroundControls]);

    // Start the skills animation when the component mounts
    useEffect(() => {
        skillsControls.start(skillsAnimationConfig);
    }, [skillsControls]);
    // Start the animation when the component mounts
    useEffect(() => {
        controls.start(animationConfig);
    }, [controls]);

    useEffect(() => {
        if (isHovered) {
            skillsControls.stop(); // Stop rotation on hover
        } else {
            skillsControls.start(skillsAnimationConfig); // Resume rotation when not hovering
        }
    }, [isHovered, skillsControls]);

    return (
        <React.Fragment>

            {/* <motion.div
                className="background-animation"
                style={{
                    position: 'fixed',
                    top: 0,
                    left: 0,
                    width: '100%',
                    height: '100%',
                    background: 'linear-gradient(180deg, #ff00ff, #00ffff)',
                    zIndex: -1, // Ensure the background is behind other content
                }}
                animate={backgroundControls}
            /> */}
            <div className="about__content">
                <div className="about__content-main">
                    <h3 className="about__content-title">Get to know me!</h3>
                    <div class="about__content-details">
                        <p class="about__content-details-para">
                            I'm a <strong>Frontend Web Developer</strong> building the
                            Front-end of Websites and Web Applications that leads to the
                            success of the overall product. Check out some of my work in the&nbsp;
                            <strong>Projects</strong> section.
                        </p>
                        <p class="about__content-details-para">
                            I also like sharing content related to the stuff that I have
                            learned over the years in <strong>Web Development</strong> so it
                            can help other people of the Dev Community. Feel free to Connect
                            or Follow me on my&nbsp;
                            <a rel="noreferrer" href="https://linkedin.com/in/rammcodes" target="_blank">Linkedin</a>
                            &nbsp;where I post useful content related to Web Development and
                            Programming
                        </p>
                        <p class="about__content-details-para">
                            I'm open to <strong>Job</strong> opportunities where I can
                            contribute, learn and grow. If you have a good opportunity that
                            matches my skills and experience then don't hesitate to&nbsp;
                            <strong>contact</strong> me.
                        </p>
                    </div>
                    <a href="./certificates" className="btn btn--med btn--theme dynamicBgClr">Contact</a>
                </div>
                <div className="about__content-skills">
                    <h3 className="about__content-title">My Skills</h3>
                    {/* <motion.div
                        className="skills"
                        //whileHover={{ rotate: 0 }}
                        whileTap={{rotate: 0}}
                        animate={controls} // Apply animation controls
                        
                    > */}
                    <div className='skills'>
                        <div className="skills__skill">HTML</div>
                        <div className="skills__skill">CSS</div>
                        <div className="skills__skill">JavaScript</div>
                        <div className="skills__skill">React</div>
                        <div className="skills__skill">SASS</div>
                        <div className="skills__skill">Github</div>
                        <div className="skills__skill">GIT</div>
                        <div className="skills__skill">Github</div>
                        <div className="skills__skill">MongoDB</div>
                        <div className="skills__skill">MySQL</div>
                        <div className="skills__skill">Jenkins</div>
                        <div className="skills__skill">Responsive Design</div>
                        <div className="skills__skill">SEO</div>
                        <div className="skills__skill">Terminal</div>
                        <div className="skills__skill">Express</div>
                    </div>

                    {/* </motion.div> */}
                </div>
            </div>

        </React.Fragment>
    );
};

export default About;
