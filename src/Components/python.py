import random
import string

def generate_random_string(length):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for _ in range(length))

# Generate 10,000 unique lines
lines = set()

while len(lines) < 10000:
    user2 = generate_random_string(8)
    user3 = generate_random_string(8)
    lastname = generate_random_string(8)
    user1 = generate_random_string(8)
    line = f"ROOT/{user2},{user1}@tt.com,{user3},{lastname}"

    # Check if the line is unique
    lines.add(line)

# Write the lines to a file or use them as needed
with open("output.txt", "w") as file:
    file.write("\n".join(lines))
