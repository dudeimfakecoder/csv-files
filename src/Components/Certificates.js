import React from 'react';
import image from '../images/Arun_image.jpeg';
const Certificates = () => {
  const divStyle = {
    backgroundImage: `url(${image})`,
    backgroundSize: 'cover',
    backgroundPosition: '30% 50%', // Adjusted to cut the first 30%
    backgroundRepeat: 'no-repeat',
    height: '100vh',
    width: '100%',
    position: 'relative', // Add position relative to allow absolute positioning of content
    color: 'white', // Add text color to ensure readability
  };

  return (
    <div class="container" style={divStyle}>
      {/* <img src={image} alt="Avatar" class="image" /> */}
      <div class="overlay">
        <h3 className='right'>Hi,</h3>
        <h2 class="titleName titlesize right">I am Arun Mallikarjun </h2>
        <h2 class="titleName right" style={{color : 'gray', position: 'relative', top : '160px'}}>Frontend Developer &amp; Designer </h2>
        <h2 class="titleName right" style={{color : 'gray', position: 'relative', top : '180px'}}>arunmarun005@gmail.com </h2>
      </div>
    </div>
  );
}

export default Certificates;
