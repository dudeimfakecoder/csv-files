import React from 'react';

const Skills = () => {
  // Create an array of your skills
  const skills = ['Skill 1', 'Skill 2', 'Skill 3', 'Skill 4', 'Skill 5'];

  return (
    <div className="skills">
      <div class="ag-format-container">
        <div class="ag-courses_box">
          <div class="ag-courses_item">
            <a href="#" class="ag-courses-item_link">
              <div class="ag-courses-item_bg"></div>

              <div class="ag-courses-item_title">
                Frontend
              </div>

              <div class="ag-courses-item_date-box">
                <div class="flex flex-wrap gap-2">
                  <div class="rounded-md bg-body text-xs text-primary p-2 border border-blue-500/60">HTML</div>
                  <div class="rounded-md bg-body text-xs text-primary p-2 border border-blue-500/60">CSS</div>
                  <div className="rounded-md bg-body text-xs text-primary p-2 border custom-border ">Javascript</div>
                  <div class="rounded-md bg-body text-xs text-primary p-2">Tailwind</div>
                  <div class="rounded-md bg-body text-xs text-primary p-2">React js</div>
                </div>
              </div>
            </a>
          </div>

          <div class="ag-courses_item">
            <a href="#" class="ag-courses-item_link">
              <div class="ag-courses-item_bg"></div>

              <div class="ag-courses-item_title">
                Backend
              </div>
              <div class="ag-courses-item_date-box">
                <div class="flex flex-wrap gap-2">
                  <div class="rounded-md bg-body text-xs text-primary p-2 border border-blue-500-60">Node Js</div>
                  <div class="rounded-md bg-body text-xs text-primary p-2 border border-gray-500/60">Express Js</div>
                  <div class="rounded-md bg-body text-xs text-primary p-2">BUN Js</div>
                  <div class="rounded-md bg-body text-xs text-primary p-2">Java</div>
                </div>
              </div>
            </a>
          </div>

          <div class="ag-courses_item">
            <a href="#" class="ag-courses-item_link">
              <div class="ag-courses-item_bg"></div>

              <div class="ag-courses-item_title">
                Database
              </div>
              <div class="ag-courses-item_date-box">
                <div class="flex flex-wrap gap-2">
                  <div class="rounded-md bg-body text-xs text-primary p-2 border border-blue-500/60">MySql</div>
                  <div class="rounded-md bg-body text-xs text-primary p-2 border border-emerald-500/60">MongoDB</div>
                  <div class="rounded-md bg-body text-xs text-primary p-2 border border-yellow-500/60">Elastic Search</div>
                </div>
              </div>
            </a>
          </div>

          <div class="ag-courses_item">
            <a href="#" class="ag-courses-item_link">
              <div class="ag-courses-item_bg"></div>

              <div class="ag-courses-item_title">
                Devops
              </div>
              <div class="ag-courses-item_date-box">
                <div class="flex flex-wrap gap-2">
                  <div class="rounded-md bg-body text-xs text-primary p-2 border border-blue-500/60">Docker</div>
                  <div class="rounded-md bg-body text-xs text-primary p-2 border border-blue-500/60">Kubernetes</div>
                  <div class="rounded-md bg-body text-xs text-primary p-2 border border-orange-500/60">GIT</div>
                  <div class="rounded-md bg-body text-xs text-primary p-2">Jenkins</div>
                  <div class="rounded-md bg-body text-xs text-primary p-2 border border-yellow-500/60">AWS</div>
                </div>
              </div>
            </a>
          </div>

        </div>
      </div>
    </div>
  );
}

export default Skills;
