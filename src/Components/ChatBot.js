import React, { useState } from 'react';

const Chatbot = () => {
    const [userMessage, setUserMessage] = useState('');
    const [chatHistory, setChatHistory] = useState([]);

    const sendMessage = async () => {
        if (userMessage.trim() === '') return;

        // Display user message
        setChatHistory([...chatHistory, { sender: 'user', message: userMessage }]);

        // Send message to the server
        try {
            const response = await fetch('http://localhost:3001/sendMessage', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ message: userMessage }),
            });

            const data = await response.json();

            // Display bot response
            setChatHistory([...chatHistory, { sender: 'bot', message: data.botResponse }]);
        } catch (error) {
            console.error('Error:', error);
        }

        // Clear user input
        setUserMessage('');
    };

    return (
        <div className="chatbot-container">
            <div className="chat-history">
                {chatHistory.map((chat, index) => (
                    <div key={index} className={chat.sender}>
                        {chat.message}
                    </div>
                ))}
            </div>
            <div className="user-input">
                <input
                    type="text"
                    placeholder="Type your message..."
                    value={userMessage}
                    onChange={(e) => setUserMessage(e.target.value)}
                />
                <button onClick={sendMessage}>Send</button>
            </div>
        </div>
    );
};

export default Chatbot;
