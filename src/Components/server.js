const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
const port = 3001;

app.use(cors());
app.use(bodyParser.json());

app.post('/sendMessage', (req, res) => {
    const userMessage = req.body.message;
    // Process the user message (you can implement your logic here)
    const botResponse = 'Your response from the server';
    res.json({ botResponse });
});

app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});
    