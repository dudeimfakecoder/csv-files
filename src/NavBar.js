import {React, useState} from 'react';
import { Link } from 'react-router-dom';
import { AppBar, Toolbar, Typography, Button } from '@mui/material';
import Home from './Components/Home';

const NavBar = () => {
    const [shapeStyle, setShapeStyle] = useState({
        left: '0px',
        width: '84.0729px',
        opacity: '1',
    });

    const getColorChange = (hello) => {
        console.log("hello", hello);
        setShapeStyle({
            left: '0px',
            width: '84.0729px',
            opacity: '1',
            backgroundColor: '#FF5733',
            borderRadius:'18px'
        });
    };
    return (
        <div>
            <Toolbar style={{ display: 'flex', justifyContent: 'space-between' }}>
                <div className='noUnderline' style={{ fontFamily: 'cursive', textDecorationLine: 'none' }}>
                    <Link to="/" className='noUnderline'>
                        Mr. Arun Mallikarjun
                    </Link>
                </div>
                <div class='menudesign'>
                    <div class='menu'>
                        <Button className='noUnderline menu_link' onClick={getColorChange} aria-current="page" style={{ color: 'white', pointerEvents: 'auto' }} component={Link} to="/">
                            Home
                        </Button>
                        <Button className='blackText noUnderline menu_link' onClick={getColorChange} style={{ color: 'white' }} component={Link} to="/about">
                            About
                        </Button>
                        <Button className='blackText noUnderline menu_link' style={{ color: 'white' }} component={Link} to="/projects">
                            Projects
                        </Button>
                        <Button className='blackText noUnderline menu_link' style={{ color: 'white' }} component={Link} to="/certificates">
                            Contact Me
                        </Button>
                        
                    </div>
                </div>
            </Toolbar>


        </div>
    );
};

export default NavBar;
